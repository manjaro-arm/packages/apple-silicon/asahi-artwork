#!/bin/bash

set -e
set -x

sizes=(16 32 48 128 256 512)

#url=https://gitlab.manjaro.org/artwork/design/-/raw/master/Manjaro_Logo_2022/logo_manjaro_rounded.svg
#mkdir -p logos/svg
#wget $url -O logos/svg/logo_manjaro_rounded.svg

for size in ${sizes[@]};
do
  mkdir -p logos/png_$size
  # create png files
  convert -background none -resize ${size}x${size} -gravity center -extent ${size}x${size} logos/svg/logo_manjaro_rounded.svg logos/png_$size/AsahiLinux_logomark.png
  # optional compression step
  #zopflipng -ym logos/png_$size/AsahiLinux_logomark.png logos/png_$size/AsahiLinux_logomark.png
done

# create icns file
mkdir -p logos/icns     
png2icns logos/icns/AsahiLinux_logomark.icns logos/png_{16,32,48,128,256,512}/AsahiLinux_logomark.png

